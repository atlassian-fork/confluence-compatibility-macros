package com.atlassian.confluence.plugins.macros.compatibility;

public class SnipsByUserMacroMigrator extends CompatibilityMacroMigrator
{
	
	private static final String SNIPS_BY_USER_PARAM_NAME = "user";
	
	@Override
	protected String getDefaultParamName()
	{
		return SNIPS_BY_USER_PARAM_NAME;
	}

}
