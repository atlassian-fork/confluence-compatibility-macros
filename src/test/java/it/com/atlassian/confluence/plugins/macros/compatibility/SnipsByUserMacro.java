package it.com.atlassian.confluence.plugins.macros.compatibility;

import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.apache.commons.lang.StringUtils;

public class SnipsByUserMacro extends AbstractCompatibilityMacroTestCase
{
    private void assertContentByUser(String userName, String ... contentTitles)
    {
        String userFullName;

        if (StringUtils.isNotEmpty(userName))
        {

            UserHelper userHelper = getUserHelper(userName);
            assertTrue(userHelper.read());

            userFullName = userHelper.getName();
        }
        else
        {
            userFullName = "Anonymous";
        }


        assertEquals(
                "Content created by " + userFullName,
                getElementTextByXPath("//div[@class='wiki-content']//table[@class='grid']//tr/th")
        );

        if (null != contentTitles && contentTitles.length > 0)
        {
            for (String contentTitle : contentTitles)
            {
                assertElementPresentByXPath(
                        "//div[@class='wiki-content']//table[@class='grid']//tr/td/a[text()='" + contentTitle + "']"
                );
            }

            assertElementNotPresentByXPath("//div[@class='wiki-content']//table[@class='grid']//tr/td/a[" + (contentTitles.length + 1) + "]");
        }
        else
        {
            assertEquals(
                    "There are no pages at the moment.",
                    getElementTextByXPath("//div[@class='wiki-content']//table[@class='grid']//tr[2]/td")
            );
        }
    }

    //TODO: fix this test. It uses contentbyuser.vm, which calls a deprecated velocity method which fails in devmode
//    public void testShowContentCreatedByAdmin()
//    {
//        long testPageId = createPage(testSpaceKey, "testShowContentCreatedByAdmin", "{snips-by-user:" + getConfluenceWebTester().getCurrentUserName() + "}");
//
//        viewPage(testPageId);
//
//        assertContentByUser(getConfluenceWebTester().getCurrentUserName(), testSpaceKey + " Home", "testShowContentCreatedByAdmin", "tst");
//    }

    public void testShowContentCreatedByAnonymousUserByDefault()
    {
        long testPageId = createPage(testSpaceKey, "testShowContentCreatedByAnonymousUserByDefault", "{snips-by-user}");

        viewPage(testPageId);

        assertContentByUser(null);
    }
}
